import static org.junit.jupiter.api.Assertions.*;
import org.junit.Test;
public class PasswordTest {
    @Test
    public void test(){
        User bob = new User(UserType.ADMIN, "Bob", "abcdefg");
        assertEquals(UserType.ADMIN, bob.getUserType());
        assertEquals("Bob", bob.getUsername());
        assertEquals("abcdefg", bob.getPassword());
    }

    @Test
    public void testLogin(){
        User bob = new User(UserType.ADMIN, "Bob", "abcdefg");
        assertTrue(true, validate(bob.getUsername(),bob.getPassword(),bob.getUserType()));
    }
}
