public class User {
    private UserType userType;
    private String username;
    private String password;
    public User(UserType userType, String username, String password){
        this.userType = userType;
        this.username = username;
        this.password = password;
    }
    public UserType getUserType(){
        return this.userType;
    }
    public String getUsername(){
        return this.username;
    }
    public String getPassword(){
        return this.password;
    }
}
