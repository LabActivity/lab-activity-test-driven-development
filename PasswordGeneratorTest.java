import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.jupiter.api.Test;

public class PasswordGeneratorTest {
    @Test
    public void genWeakPWTest1() {
        String phrase = "TestMe";
        PasswordGenerator pass = new PasswordGenerator(phrase);
        assertEquals(pass.genWeakPW().length(), phrase.length());
    }

    @Test
    public void genWeakPWTest2() {
        String phrase = "TestMe";
        PasswordGenerator pass = new PasswordGenerator(phrase);
        assertTrue(noCapTrue(pass.genWeakPW()));

    }

    private boolean noCapTrue(String pass) {
        int count = 0;
        for (int i = 0; i < pass.length(); i++) {
            if (pass.charAt(i) > 'a' && pass.charAt(i) < 'z') {
                count++;
            }
        }
        if (count > 0) {
            return true;
        } else
            return false;
    }

    @Test
    public void genStrongPWTest1() {
        String phrase = "TestMe";
        PasswordGenerator pass = new PasswordGenerator(phrase);
        assertEquals(pass.genStrongPW().length(), (phrase.length() * 2));
    }
    @Test
    public void  genStrongPWTest2() {
        String phrase = "TestMe";
        PasswordGenerator pass = new PasswordGenerator(phrase);
        assertTrue(capTrue(pass.genStrongPW())); 

    }

    private boolean capTrue(String pass) {
        int count = 0;
        for (int i = 0; i < pass.length(); i++) {
            if (pass.charAt(i) >= 'A' && pass.charAt(i) <= 'Z') {
                count++;
            }
        }
        if (count > 0) {
            return true;
        } else
            return false;
    }
    @Test
    public void  genStrongPWTest3() {
        String phrase = "TestMe";
        PasswordGenerator pass = new PasswordGenerator(phrase);
        assertTrue(numTrue(pass.genStrongPW())); 

    }

    private static boolean numTrue(String pass) {
        int count = 0;
        for (int i = 0; i < pass.length(); i++) {
            if (pass.charAt(i) >= '0' && pass.charAt(i) <= '9') {
                count++;
            }
        }
        System.out.println(count);
        if (count > 0) {
            return true;
        } else
            return false;
    }
}
