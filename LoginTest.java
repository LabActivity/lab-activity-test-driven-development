import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import org.junit.jupiter.api.Test;
public class LoginTest {
    @Test
    public void testUserCreation(){
        User[] slaves = new User[3];
        slaves[0] = new User(UserType.ADMIN, "Marco", "0123456789");
        slaves[1] = new User(UserType.REGULAR, "James", "abcdefg");
        slaves[2] = new User(UserType.REGULAR, "Lily", "147852");
    }

    @Test
    public void testUserGetMethods(){
        User[] slaves = new User[3];
        slaves[0] = new User(UserType.ADMIN, "Marco", "0123456789");
        slaves[1] = new User(UserType.REGULAR, "James", "abcdefg");
        slaves[2] = new User(UserType.REGULAR, "Lily", "147852");

        assertEquals(slaves[0].getUserType(), UserType.ADMIN);
        assertEquals("Marco", slaves[0].getUsername());
    }

    @Test
    public void testValidate(){
        User[] slaves = new User[3];
        slaves[0] = new User(UserType.ADMIN, "Marco", "0123456789");
        slaves[1] = new User(UserType.REGULAR, "James", "abcdefg");
        slaves[2] = new User(UserType.REGULAR, "Lily", "147852");
        Login company = new Login(slaves);
        assertTrue(company.validate("Marco", "0123456789", true));
        assertTrue(company.validate("Lily", "147852", false));
        assertTrue(company.validate("James", "abcdefg", false));
    }
}
