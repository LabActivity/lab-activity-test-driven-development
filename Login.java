public class Login {
	private User[] userList;

	public Login(User[] user) {
		this.userList = user;
	}

	public boolean validate(String username, String password, boolean admin) {

		if (admin == true) {
			boolean passingValidation;
			int returnIndex = matchCredentialForType(username, password);
			passingValidation = matchUserType(returnIndex);
			return passingValidation;
		}

		return matchCredential(username, password);
	}

	public boolean matchCredential(String username, String password) {
		for (int i = 0; i < this.userList.length; i++) {
			if (username.equals(this.userList[i].getUsername())) {
				if (password.equals(this.userList[i].getPassword())) {
					return true;
				}
			}
		}
		return false;
	}

	public int matchCredentialForType(String username, String password) {
		final int NOMATCH = -1;
		for (int i = 0; i < this.userList.length; i++) {
			if (username.equals(this.userList[i].getUsername())) {
				if (password.equals(this.userList[i].getPassword())) {
					return i;
				}
			}
		}
		return NOMATCH;
	}

	public boolean matchUserType(int index) {
		final int NOMATCH = -1;
		if (index > NOMATCH) {
			if (UserType.ADMIN.equals(this.userList[index].getUserType())) {
				return true;
			}
		}

		return false;
	}
}