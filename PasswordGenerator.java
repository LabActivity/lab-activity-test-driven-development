import java.util.Random;
public class PasswordGenerator {
    private int pwLength;
    private Random rng = new Random();
    final int MAX_ALPHA = 26;
    final String LOW_ALPHA = "abcdefghijklmnopqrstuvwxyz";
    final String CAP_ALPHA = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    final String NUMBERS = "0123456789";
    
    public PasswordGenerator(String password){
        this.pwLength = password.length();
    }

    public String genWeakPW(){
        StringBuilder pw = new StringBuilder();
        for (int i = 0; i < pwLength; i++){
            pw.append(addLowChar());
        }
        return pw.toString();
    }

    public String genStrongPW(){
        StringBuilder pw = new StringBuilder();
        for (int i = 0; i < pwLength*2; i++){
            int caseFlag = rng.nextInt(2);
            if (caseFlag == 0){
                pw.append(addLowChar());
            }
            else if(caseFlag == 1){
                pw.append(addNumbChar());
            }
            else{
                pw.append(addCapChar());
            }
        }
        return pw.toString();
    }

    private char addLowChar(){
        int num = rng.nextInt(MAX_ALPHA);
        return LOW_ALPHA.charAt(num);
    }

    private char addCapChar(){
        int num = rng.nextInt(MAX_ALPHA);
        return CAP_ALPHA.charAt(num);
    }

    private char addNumbChar(){
        int num = rng.nextInt(10);
        return NUMBERS.charAt(num);
    }
}
